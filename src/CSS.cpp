#include "CSS.h"

String webpage = "";

void append_page_header()
{
    webpage = F("<!DOCTYPE html><html>");
    webpage += F("<head>");
    webpage += F("<title>SophiBox</title>"); // NOTE: 1em = 16px
    webpage += F("<meta http-equiv='content-type' content='text/html'; charset=utf-8>");
    webpage += F("<meta name='viewport' content='user-scalable=yes,initial-scale=1.0,width=device-width'>");
    webpage += F("<style>");
    webpage += F("body{max-width:65%;margin:0 auto;font-family:arial;font-size:105%;text-align:center;color:blue;background-color:#F7F2Fd;}");
    webpage += F("ul{list-style-type:none;margin:0.1em;padding:0;border-radius:0.375em;overflow:hidden;background-color:#dcade6;font-size:1em;}");
    webpage += F("li{float:left;border-radius:0.375em;border-right:0.06em solid #bbb;}last-child {border-right:none;font-size:85%}");
    webpage += F("li a{display: block;border-radius:0.375em;padding:0.44em 0.44em;text-decoration:none;font-size:85%}");
    webpage += F("li a:hover{background-color:#EAE3EA;border-radius:0.375em;font-size:85%}");
    webpage += F("section {font-size:0.88em;}");
    webpage += F("h1{color:white;border-radius:0.5em;font-size:1em;padding:0.2em 0.2em;background:#558ED5;}");
    webpage += F("h2{color:orange;font-size:1.0em;}");
    webpage += F("h3{font-size:0.8em;}");
    webpage += F("table{font-family:arial,sans-serif;font-size:0.9em;border-collapse:collapse;width:85%;}");
    webpage += F("th,td {border:0.06em solid #dddddd;text-align:left;padding:0.3em;border-bottom:0.06em solid #dddddd;}");
    webpage += F("tr:nth-child(odd) {background-color:#eeeeee;}");
    webpage += F(".rcorners_n {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:20%;color:white;font-size:75%;}");
    webpage += F(".rcorners_m {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:50%;color:white;font-size:75%;}");
    webpage += F(".rcorners_w {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:70%;color:white;font-size:75%;}");
    webpage += F(".column{float:left;width:50%;height:45%;}");
    webpage += F(".row:after{content:'';display:table;clear:both;}");
    webpage += F("*{box-sizing:border-box;}");
    webpage += F("footer{background-color:#eedfff; text-align:center;padding:0.3em 0.3em;border-radius:0.375em;font-size:60%;}");
    webpage += F("button{border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:20%;color:white;font-size:130%;}");
    webpage += F(".buttons {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:15%;color:white;font-size:80%;}");
    webpage += F(".buttonsm{border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:9%; color:white;font-size:70%;}");
    webpage += F(".buttonm {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:15%;color:white;font-size:70%;}");
    webpage += F(".buttonw {border-radius:0.5em;background:#558ED5;padding:0.3em 0.3em;width:40%;color:white;font-size:70%;}");
    webpage += F("a{font-size:75%;}");
    webpage += F("p{font-size:75%;}");
    webpage += F(".headerSortDown:after,");
    webpage += F(".headerSortUp:after{");
    webpage += F("content: ' ';");
    webpage += F("position: relative;");
    webpage += F("left: 0px;");
    webpage += F("border: 7px solid transparent;");
    webpage += F("}");
    webpage += F(".headerSortDown:after{");
    webpage += F("top: 10px;");
    webpage += F("border-top-color: silver;");
    webpage += F("}");
    webpage += F(".headerSortUp:after{");
    webpage += F("bottom: 10px;");
    webpage += F("border-bottom-color: silver;");
    webpage += F("}");
    webpage += F(".headerSortDown,");
    webpage += F(".headerSortUp{");
    webpage += F("padding-left: 10px;");
    webpage += F("}");
    webpage += F("table {");
    webpage += F("width: 100%;");
    webpage += F("border: 1px solid #000;");
    webpage += F("}");
    webpage += F("th.select {");
    webpage += F("width: 5%;");
    webpage += F("}");
    webpage += F("th.order {");
    webpage += F("width: 5%;");
    webpage += F("}");
    webpage += F("</style></head><body><h1>SophiBox ");
    webpage += String(ServerVersion) + "</h1>";
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void append_page_footer(String cur_dir)
{ // Saves repeating many lines of code for HTML page footers
    cur_dir.replace("/", "%2F");
    webpage += F("<ul>");
    webpage += F("<li><a href='/'>Home</a></li>"); // Lower Menu bar command entries
    webpage += F("<li><a href='/dir'>Root</a></li>");
    webpage += "<li><a href='/mkdir?parent=" + cur_dir + "'>Create Folder</a></li>";
    webpage += "<li><a href='/upload?parent=" + cur_dir + "'>Upload</a></li>";
    webpage += F("<li><a href=\"javascript:;\" onclick=\"if (confirm('Do you really want to delete the selected file(s)?')) {document.getElementById('selected').submit();}\">Delete</a></li>");
    webpage += "<li><a href='/link?parent=" + cur_dir + "'>Link to RFID</a></li>";
    webpage += F("<li><a href='/exit?'>Exit</a></li>");
    webpage += F("</ul>");
    webpage += "<footer>&copy; Bodenstedt Data Science</footer>";
    webpage += F("</body></html>");
}