#include "AudioPlayer.h"
#include <random>

void AudioPlayer::SetCard(Card card)
{
    uint32_t id = card.ID;
    this->m_CardID = id;
    
    String path = this->m_CardManager->GetPath(this->m_CardID);
    this->m_CurrentMode = this->m_CardManager->GetMode(this->m_CardID);

    uint32_t c_pos;
    this->m_CardManager->GetPosition(this->m_CardID, this->m_CurrentTrack, c_pos);

    Serial.printf("Loading card with id %u mode %u, file %u Pos %u\n", this->m_CardID, this->m_CurrentMode, this->m_CurrentTrack, c_pos);
    strVec dirs;
    fileVec files;

    this->m_SDReader->BrowseAndMove(path.c_str(), dirs, files);
    Serial.print("Card found, path: ");
    Serial.println(path);

    this->m_Files.clear();
    for (fileVec::iterator it = files.begin(); it != files.end();it++)
    {
        //TODO: Extension check
        String file = path;
        if (file[file.length() - 1] != '/')
            file += "/";
        file += it->name;
            
        this->m_Files.push_back(file);
        Serial.println(file);
    }
    
    if (this->m_CurrentTrack == 65535)
    {
        auto rng = std::default_random_engine(millis());
        std::shuffle(std::begin(this->m_Files), std::end(this->m_Files), rng);
        this->m_CurrentTrack = 0;
    }

    Serial.printf("Playing file %d: %s starting at %d\n", this->m_CurrentTrack,this->m_Files[this->m_CurrentTrack].c_str() ,c_pos);
    xSemaphoreTake(SPIMutex, portMAX_DELAY); // enter critical section
    this->m_Audio.connecttoSD(this->m_Files[this->m_CurrentTrack].c_str() ,c_pos);
    xSemaphoreGive(SPIMutex);
    this->SetPlaying(true);
}

void AudioPlayer::SetVolume(uint8_t volume)
{
    this->m_Audio.setVolume(volume);
}

void AudioPlayer::Restart()
{
    if (!this->IsPlaying())
        return;

    if ((this->m_CurrentTrack == 0)&&(this->m_Audio.getAudioCurrentTime() < 10))
        return;

    if (this->m_CurrentMode == 2)
    {
        auto rng = std::default_random_engine(millis());
        std::shuffle(std::begin(this->m_Files), std::end(this->m_Files), rng);
    }

    this->m_CurrentTrack = 0;
    
    this->m_CardManager->SetPosition(this->m_CardID, 0, 0);

    this->m_Audio.connecttoSD(this->m_Files[this->m_CurrentTrack].c_str());
}

void AudioPlayer::Stop()
{
    this->SaveState();
    this->SetPlaying(false);
    this->m_Audio.stopSong();
}

void AudioPlayer::Back()
{
    if (!this->IsPlaying())
        return;

    if (this->m_Audio.getAudioCurrentTime() > THRS_REWIND)
    {
        this->m_Audio.setAudioPlayPosition(0);
        return;
    }

    if (this->m_CurrentTrack - 1 >= 0)
    {
        this->m_CurrentTrack--;
    }
    else
    {
        this->m_CurrentTrack = this->m_Files.size() - 1;
    }

    this->m_Audio.connecttoSD(this->m_Files[this->m_CurrentTrack].c_str());
}

void AudioPlayer::Next(bool manual)
{
    if (!this->IsPlaying())
        return;

    if (this->m_CurrentTrack + 1 < this->m_Files.size())
    {
        this->m_CurrentTrack++;
    }
    else
    {
        if ((manual)||(this->m_CurrentMode == 2))
            this->m_CurrentTrack = 0;
        else
        {
            this->SetPlaying(false);
            this->m_CardManager->SetPosition(this->m_CardID, 0, 0);
            return;
        }
    }


    this->m_Audio.connecttoSD(this->m_Files[this->m_CurrentTrack].c_str());
}

void AudioPlayer::Rewind()
{
    if (!this->IsPlaying())
        return;

    uint32_t cur_pos = this->m_Audio.getFilePos();
    uint32_t new_pos =  cur_pos - 2*(FASTFOWARD_TIME*this->m_Audio.getSampleRate()/8);
    uint32_t min_pos = this->m_Audio.getAudioDataStartPos();
    /*
    uint32_t new_time = this->m_Audio.getAudioCurrentTime() + FASTFOWARD_TIME;
    Serial.printf("Fastforward from %u to %u from %u, %u\n", this->m_Audio.getAudioCurrentTime(), new_time, this->m_Audio.getTotalPlayingTime(), this->m_Audio.getSampleRate());
    if (new_time < this->m_Audio.getTotalPlayingTime())
    {
        bool res = this->m_Audio.setAudioPlayPosition(new_time);
        Serial.printf("Fastforward %d\n", res);
    }
    */
    Serial.printf("Rewind from %u to %u, min %u, cur %u\n", cur_pos, new_pos, min_pos, this->m_Audio.getAudioCurrentTime());
    if (cur_pos > min_pos)
    {
        this->m_Audio.setFilePos(new_pos);
    }
    else
    {
        this->Next();
        Serial.println("Going next\n");
    }
}

void AudioPlayer::FastForward()
{
    if (!this->IsPlaying())
        return;
    
    uint32_t cur_pos = this->m_Audio.getFilePos();
    uint32_t new_pos =  cur_pos + (FASTFOWARD_TIME*this->m_Audio.getSampleRate()/8);
    uint32_t max_pos = this->m_Audio.getFileSize();
    /*
    uint32_t new_time = this->m_Audio.getAudioCurrentTime() + FASTFOWARD_TIME;
    Serial.printf("Fastforward from %u to %u from %u, %u\n", this->m_Audio.getAudioCurrentTime(), new_time, this->m_Audio.getTotalPlayingTime(), this->m_Audio.getSampleRate());
    if (new_time < this->m_Audio.getTotalPlayingTime())
    {
        bool res = this->m_Audio.setAudioPlayPosition(new_time);
        Serial.printf("Fastforward %d\n", res);
    }
    */
    Serial.printf("Fastforward from %u to %u, max %u, cur %u\n", cur_pos, new_pos, max_pos, this->m_Audio.getAudioCurrentTime());
    if (cur_pos < max_pos)
    {
        this->m_Audio.setFilePos(new_pos);
    }
    else
    {
        this->Next();
        Serial.println("Going next\n");
    }
}

bool AudioPlayer::IsPlaying()
{
    xSemaphoreTake(this->m_Mutex, portMAX_DELAY); // enter critical section
    bool playing = this->m_Playing;
    xSemaphoreGive(this->m_Mutex); // exit critical section

    return playing;
}

void AudioPlayer::SetPlaying(bool play)
{
    xSemaphoreTake(this->m_Mutex, portMAX_DELAY); // enter critical section
    this->m_Playing = play;
    xSemaphoreGive(this->m_Mutex); // exit critical section
}

void AudioPlayer::SaveState()
{
    this->m_CardManager->SetPosition(this->m_CardID, this->m_CurrentTrack, this->m_Audio.getFilePos());
}

void AudioPlayer::ThreadFunction(void* pvParameters)
{
    AudioPlayer* parent = (AudioPlayer*)pvParameters;

    while (parent->m_ThreadRunning)
    {
        parent->Loop();
        delay(1);
    }
}

void AudioPlayer::Loop()
{
    //this->m_Audio.isRunning();
    bool playing = this->IsPlaying();
    if (playing)
    {
        xSemaphoreTake(SPIMutex, portMAX_DELAY); // enter critical section
        this->m_Audio.loop();
        xSemaphoreGive(SPIMutex); // exit critical section
        unsigned long curTime = millis();
        if (curTime - this->m_LastSave > TIME_TO_SAVE)
        {
            this->m_LastSave = curTime;
            this->SaveState();
        }
    }
    else
    {
        delay(500);
    }
}

AudioPlayer::AudioPlayer(uint8_t BCLK, uint8_t LRC, uint8_t DOUT, SDCardReader* sdReader)
{
    this->m_Audio.setPinout(BCLK, LRC, DOUT);
    Serial.println("Pintout set");
    
    this->m_ThreadRunning = true;
    this->m_Mutex = xSemaphoreCreateMutex();
    BaseType_t xReturned;
    this->m_Playing = false;
        
    assert(this->m_Mutex);
    Serial.println("Mutex set");
    
    this->m_SDReader = sdReader;
    Serial.println("SD set");
    this->m_CardManager = RFIDCardManager::getInstance();
    Serial.println("RFID set");

    this->m_Audio.setVolumeSteps(100);
    this->m_Audio.setVolume(5);

    this->m_LastSave = 0;
    /*
    xReturned = xTaskCreate(
                    AudioPlayer::ThreadFunction,
                    "AudioPlayer",
                    STACK_SIZE,
                    ( void * ) this,
                    tskIDLE_PRIORITY + 2,
                    &(this->m_ThreadHandle));
    Serial.println("Thread set");*/
}

AudioPlayer::~AudioPlayer()
{
    
}
