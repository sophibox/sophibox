#include "LEDs.h"
#include <assert.h>
#include <esp32-hal-ledc.h>

LED::LED(uint8_t channel, uint8_t pin, uint8_t init_brightness, uint32_t frequency)
{
    assert((channel >= 0) && (channel < 16));
    this->m_Channel = channel;
    this->m_Pin = pin;

    ledcSetup(channel, frequency, 8);

    ledcAttachPin(pin, channel);

    this->SetBrightness(init_brightness);
    this->m_LastBlink = 0;
    this->m_BlinkWait = 0;
}

void LED::SetBrightness(uint8_t brightness)
{
    this->m_Brightness = brightness;
    this->m_CurrentBrightness = brightness;
    ledcWrite(this->m_Channel, this->m_Brightness);
}

uint8_t LED::GetBrightness()
{
    return this->m_Brightness;    
}

void LED::SetBlinking(unsigned long wait)
{
    this->m_LastBlink = millis();
    this->m_BlinkWait = wait;
}

void LED::SetLightMode(const LightMode & mode)
{
    this->SetBrightness(mode.brightness);
    this->SetBlinking(mode.blinking);
}

void LED::Update()
{
    if (this->m_BlinkWait  == 0)
        return;
    
    unsigned long curTime = millis();

    if (curTime - this->m_LastBlink > this->m_BlinkWait)
    {
        unsigned long newBrightness = 0;
        if (this->m_CurrentBrightness == 0)
        {
            newBrightness = this->m_Brightness;
        }

        ledcWrite(this->m_Channel, newBrightness);
        this->m_CurrentBrightness = newBrightness;
        this->m_LastBlink = curTime;    
    }
}

LED::~LED()
{
    ledcDetachPin(this->m_Pin);
}