#include "SDCard.h"
#include <SD.h>

SDCardReader::SDCardReader(uint8_t chipSelectPin)
{
    xSemaphoreTake(SPIMutex, portMAX_DELAY);
    this->m_SDPresent = true;
    if (!SD.begin(chipSelectPin))
    {
        Serial.println("Card Mount Failed");
        this->m_SDPresent = false;
        xSemaphoreGive(SPIMutex);
        return;
    }
    this->m_CardType = SD.cardType();

    if (this->m_CardType == CARD_NONE)
    {
        Serial.println("No SD card attached");
        this->m_SDPresent = false;
        xSemaphoreGive(SPIMutex);
        return;
    }

    Serial.print("SD Card Type: ");
    if (this->m_CardType == CARD_MMC)
    {
        Serial.println("MMC");
    }
    else if (this->m_CardType == CARD_SD)
    {
        Serial.println("SDSC");
    }
    else if (this->m_CardType == CARD_SDHC)
    {
        Serial.println("SDHC");
    }
    else
    {
        Serial.println("UNKNOWN");
    }

    this->m_CardSize = SD.cardSize() / (1024 * 1024);
    Serial.printf("SD Card Size: %lluMB\n", this->m_CardSize);

    // listDir(SD, "/", 2);

    Serial.printf("Total space: %lluMB\n", SD.totalBytes() / (1024 * 1024));
    Serial.printf("Used space: %lluMB\n", SD.usedBytes() / (1024 * 1024));
    xSemaphoreGive(SPIMutex);
}

SDCardReader::~SDCardReader()
{
    SD.end();
}

void SDCardReader::Delete(const String &path)
{
    xSemaphoreTake(SPIMutex, portMAX_DELAY);
    if (this->m_SDPresent)
    {
        File dataFile = SD.open(path.c_str(), FILE_WRITE); // Now read data from SD Card
        Serial.print("Deleting file: ");
        Serial.println(path);
        if (dataFile)
        {
            if (dataFile.isDirectory())
            {
                Serial.println("Delete folder");
                dataFile.close();
                this->DeleteRecursive(path);
                // SD.rmdir(filename);
            }
            else
            {
                Serial.println("Delete file");
                dataFile.close();
                SD.remove(path.c_str());
            }
        }
        else
        {
            Serial.println("Couldn't open");
        }
    }
    xSemaphoreGive(SPIMutex);
}

void SDCardReader::DeleteRecursive(const String &path)
{
    xSemaphoreTake(SPIMutex, portMAX_DELAY);
    File dir = SD.open(path.c_str(), FILE_WRITE);
    dir.rewindDirectory();
    while (true)
    {
        File nextDir = dir.openNextFile();
        if (!nextDir)
        {
            // no more, delete the directory
            Serial.print("Removing folder ");
            Serial.print(dir.name());
            if (SD.rmdir(path.c_str()))
            {
                Serial.println(" - Sucess");
            }
            else
            {
                Serial.println(" - Fail");
            }
            break;
        }
        else
        {
            if (nextDir.isDirectory())
            {
                this->DeleteRecursive(path + "/" + nextDir.name());
            }
            else
            {
                // delete the file
                Serial.print("Removing file ");
                Serial.print(nextDir.name());
                if (SD.remove(String(path + "/" + nextDir.name()).c_str()))
                {
                    Serial.println(" - Sucess");
                }
                else
                {
                    Serial.println(" - Fail");
                }
            }
        }
    }
    xSemaphoreGive(SPIMutex);
}

void SDCardReader::BrowseAndMove(const char *dirname, strVec &dirs, fileVec &files, int move, String moveFile)
{
    xSemaphoreTake(SPIMutex, portMAX_DELAY);
    File root = SD.open(dirname);

    // preferences.begin("FileOrder", false);
    // String order = preferences.getString(dirname, "");
    char configPath[512];
    String order;
    sprintf(configPath, "%s/order.cfg");
    if (SD.exists(configPath))
    {
        fs::File file = SD.open(configPath);
        order = file.readString();
        file.close();
    }
    Serial.print("Read ");
    Serial.println(order);

    int pos = 0;
    String token;

    std::map<String, int> orderMap;

    String orig_order = order;

    while ((pos = order.indexOf('|')) != -1)
    {
        token = order.substring(0, pos);
        int pos2 = order.indexOf(':');
        Serial.println(token.substring(0, pos2));
        Serial.println(token.substring(pos2 + 1, pos));

        orderMap[order.substring(0, pos2)] = order.substring(pos2 + 1, pos).toInt();
        order = order.substring(pos + 1);
    }

#ifdef ESP8266
    root.rewindDirectory(); // Only needed for ESP8266
#endif
    if (!root)
    {
        xSemaphoreGive(SPIMutex);
        return;
    }
    if (!root.isDirectory())
    {
        xSemaphoreGive(SPIMutex);
        return;
    }
    File file = root.openNextFile();
    fileVec sortedFiles;
    while (file)
    {
        if (String(file.name()) == "order.cfg")
        {
            file = root.openNextFile();
            continue;
        }
        Serial.println(String(file.isDirectory() ? "Dir " : "File ") + String(file.name()));
        if (file.isDirectory())
        {
            dirs.push_back(file.name());
        }
        else
        {
            std::map<String, int>::iterator it = orderMap.find(file.name());

            fileDescriptor fd;
            fd.name = file.name();
            fd.size = file.size();

            if (it != orderMap.end())
                sortedFiles.push_back(fd);
            else
                files.push_back(fd);
        }
        file = root.openNextFile();
    }
    file.close();

    fileSorter sorter(orderMap);

    std::sort(dirs.begin(), dirs.end());
    std::sort(sortedFiles.begin(), sortedFiles.end(), sorter);
    std::sort(files.begin(), files.end(), [](fileDescriptor a, fileDescriptor b)
              { return String(a.name) < String(b.name); });

    files.insert(files.begin(), sortedFiles.begin(), sortedFiles.end());

    if (move > 0)
    {
        for (size_t i = 0; i < files.size(); i++)
        {
            if (String(files[i].name) == moveFile)
            {
                if (move == 1)
                {
                    if (i > 0)
                        std::iter_swap(files.begin() + i, files.begin() + i - 1);
                }
                else
                {
                    if (i < files.size() - 1)
                        std::iter_swap(files.begin() + i, files.begin() + i + 1);
                }
                break;
            }
        }
    }

    order = "";
    for (size_t i = 0; i < files.size(); i++)
    {
        order += String(files[i].name) + ":" + String(i) + "|";
    }

    // preferences.putString(dirname, order);

    // preferences.end();

    if (order != orig_order)
    {
        Serial.print("Wrote ");
        Serial.println(order);
        file = SD.open(configPath, "w", true);
        file.print(order);
        file.close();
    }
    xSemaphoreGive(SPIMutex);
}

bool SDCardReader::OpenFile(const String &path)
{

    if (IsOpened(path))
        return true;
    xSemaphoreTake(SPIMutex, portMAX_DELAY);
    File f = SD.open(path.c_str(), FILE_WRITE);
    if (!f)
    {
        xSemaphoreGive(SPIMutex);
        return false;
    }

    this->m_OpenFiles[path] = f;
    xSemaphoreGive(SPIMutex);
    return true;
}

bool SDCardReader::WriteFile(const String &path, const uint8_t *data, size_t size)
{

    if (!IsOpened(path))
        return false;
    xSemaphoreTake(SPIMutex, portMAX_DELAY);
    bool ret = this->m_OpenFiles[path].write(data, size) > 0;
    xSemaphoreGive(SPIMutex);
    return ret;
}

bool SDCardReader::CloseFile(const String &path)
{

    if (!IsOpened(path))
        return false;
    xSemaphoreTake(SPIMutex, portMAX_DELAY);
    this->m_OpenFiles[path].close();

    this->m_OpenFiles.erase(path);
    xSemaphoreGive(SPIMutex);
    return true;
}

bool SDCardReader::MakeDir(const String &path)
{
    xSemaphoreTake(SPIMutex, portMAX_DELAY);
    bool ret = SD.mkdir(path.c_str());
    xSemaphoreGive(SPIMutex);
    return ret;
}

bool SDCardReader::IsOpened(const String &path)
{
    std::map<String, File>::iterator it = this->m_OpenFiles.find(path);

    return it != this->m_OpenFiles.end();
}
