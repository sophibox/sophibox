#include "WebInterface.h"
#include "RFID.h"

WebInterface::WebInterface(SDCardReader *sdReader, RFIDCardReader *rfidReader)
{
    this->m_SDReader = sdReader;
    this->m_RFIDReader = rfidReader;

    std::vector<const char *> menuIds = {"custom", "wifi", "info", "sep", "exit","update"};
    this->m_WM.setCustomMenuHTML("<form action='/dir'   method='get'><button>File Manager</button></form><br/>\n"
                        "<form action='/settings'   method='get'><button>Settings</button></form><br/>\n"
                        "<form action='/cfgrfid'   method='get'><button>Create Admin Tag</button></form><br/>\n"
                        "<form action='/freset'   method='get'><button>Factory Reset</button></form><br/>\n"
                        );
    this->m_WM.setMenu(menuIds);

    this->m_Timeout = 1000 * 60;
}

void WebInterface::ActivateInterface()
{
    WiFi.mode(WIFI_STA); // explicitly set mode, esp defaults to STA+AP

    String hostname = Settings::getInstance()->GetHostname();
    String password = Settings::getInstance()->GetPassword();

    WiFi.setHostname(hostname.c_str());
    this->m_WM.setHostname(hostname.c_str());

    bool res;

    res = this->m_WM.autoConnect(hostname.c_str(), password.c_str()); // password protected ap

    if (!res)
    {
        Serial.println("Failed to connect");
        // ESP.restart();
    }
    else
    {
        // if you get here you have connected to the WiFi
        Serial.println("connected...yeey :)");
    }

    this->m_WM.startWebPortal();

    this->m_Server = this->m_WM.server.get();

    this->m_Server->stop();

    this->m_PFileUpload = std::bind(&WebInterface::File_Upload, this);
    this->m_PHandleFileUpload = std::bind(&WebInterface::Handle_File_Upload, this);
    this->m_PFileDelete = std::bind(&WebInterface::File_Delete, this);
    this->m_PListDir = std::bind(&WebInterface::List_Dir, this);
    this->m_PCreateDir = std::bind(&WebInterface::Create_Dir, this);
    this->m_PLinkRFID = std::bind(&WebInterface::LinkRFID, this);
    this->m_CreateConfigRFID = std::bind(&WebInterface::CreateConfigRFID, this);
    this->m_PHandleFileUpload2 = std::bind([](WebInterface *p)
                                           { p->m_Server->send(200); },
                                           this);
    this->m_PSettings = std::bind(&WebInterface::Settings, this);
    this->m_PFReset = std::bind(&WebInterface::FactoryReset, this);

    this->m_Server->on("/upload", this->m_PFileUpload);
    this->m_Server->on("/fupload", HTTP_POST, this->m_PHandleFileUpload2, this->m_PHandleFileUpload);
    this->m_Server->on("/delete", this->m_PFileDelete);
    this->m_Server->on("/dir", this->m_PListDir);
    this->m_Server->on("/mkdir", this->m_PCreateDir);
    this->m_Server->on("/link", this->m_PLinkRFID);
    this->m_Server->on("/cfgrfid", this->m_CreateConfigRFID);
    this->m_Server->on("/settings", this->m_PSettings);
    this->m_Server->on("/freset", this->m_PFReset);

    const char * headerkeys[] = {"Content-Length"} ;
    size_t headerkeyssize = sizeof(headerkeys) / sizeof(char*);
    //ask server to track this headers
    this->m_Server->collectHeaders(headerkeys, headerkeyssize);

    this->m_Server->begin();
    Serial.println("HTTP server started");
    this->m_WM.handleRequest();
}

void WebInterface::DeactivateInterface()
{
    this->m_Server->stop();
    Serial.println("Stopped server");
    this->m_WM.stopWebPortal();
    Serial.println("Stopped portal");
    this->m_WM.disconnect();
    Serial.println("Stopped wm");
    // WiFi.disconnect(true);
}

bool WebInterface::Loop()
{
    this->m_WM.process();

    unsigned long t = this->m_WM.GetPortalAccessedTime();

    unsigned long t2 = millis();

    Card currentCard = this->m_RFIDReader->GetCurrentCard();

    if (t2 - t > this->m_Timeout)
    {
        if ((currentCard.BlockData[WEBPORTAL_BYTE]) || (!RFIDCardManager::getInstance()->Configured(currentCard.ID)&& (currentCard.ID != NO_CARD_ID)))
        {
            return this->m_WM.getWebPortalActive();
        }
        Serial.printf("Portal timed out! Portal Byte %d, configured %d, no card %d\n", currentCard.BlockData[WEBPORTAL_BYTE], RFIDCardManager::getInstance()->Configured(currentCard.ID), currentCard.ID == NO_CARD_ID);
        return false;
    }

    return this->m_WM.getWebPortalActive();
}

void WebInterface::File_Upload()
{
    Serial.println("File upload stage-1");
    append_page_header();
    String parent = this->m_Server->arg(0);
    /*
    webpage += F("<h3>Select File to Upload</h3>");
    webpage += F("<FORM action='/fupload' method='post' enctype='multipart/form-data'>");
    webpage += "<input type='hidden' name='parent' value='" + parent + "' />";
    webpage += F("<input type='hidden' name='fileCount' id='fileCount' value='' />");
    webpage += F("<input class='buttons' style='width:40%' type='file' name='fupload[]' id = 'fupload' value='' accept='audio/*' multiple onchange='onChange()'><br>");
    webpage += F("<br><button class='buttons' style='width:10%' type='submit'>Upload File</button><br></form>");
    webpage += "<a href='/dir?dir=" + parent + "'>[Back]</a><br><br>";
    webpage += "<script>";
    webpage += "function onChange() {";
    webpage += "var numFiles = document.getElementById('fupload').files.length;";
    webpage += "document.getElementById(\"fileCount\").value = numFiles;";
    webpage += "}";
    webpage += "</script>";
    */
    webpage += F("<form action='' method='post' enctype='multipart/form-data'>\n");
webpage += F("<input name='file' type='file' id='fileA' accept='audio/*,.bin' multiple/>\n");
webpage += F("<input name='upload' value='Upload' type='button' onclick='uploadFile();' />\n");
webpage += F("</form>\n");
webpage += F("<div>\n");
webpage += F("<div id='fileName'></div>\n");
webpage += F("<div id='fileSize'></div>\n");
webpage += F("<div id='fileType'></div>\n");
webpage += F("<progress id='progress' style='margin-top:10px'></progress> <span id='prozent'></span>\n");
webpage += F("</div>\n");
webpage += "<a href='/dir?dir=" + parent + "'>[Back]</a><br><br>";
webpage += F("</body>\n");
webpage += F("<script>\n");
webpage += F("var client = null;\n");
webpage += F("function sleep(ms) {\n");
webpage += F("return new Promise(resolve => setTimeout(resolve, ms));\n");
webpage += F("}\n");
webpage += F("function uploadFile(){\n");
webpage += F("for (var i = 0; i < document.getElementById('fileA').files.length; i++) {");
webpage += F("sleep(1000);\n");
webpage += F("var file = document.getElementById('fileA').files[i];\n");
webpage += F("var formData = new FormData();\n");
webpage += F("client = new XMLHttpRequest();\n");
webpage += F("var prog = document.getElementById('progress');\n");
webpage += F("if(!file)\n");
webpage += F("return;\n");
webpage += F("prog.value = 0;\n");
webpage += F("prog.max = 100;\n");
webpage += "formData.append('parent', '"  + parent + "');\n";
webpage += F("formData.append('fileCount', document.getElementById('fileA').files.length);\n");
webpage += F("formData.append('file', file);\n");
//webpage += F("document.getElementById('fileName').innerHTML = 'Uploading: ' + file.name;\n");
//webpage += F("document.getElementById('fileSize').innerHTML = 'Size: ' + file.size + ' B';\n");
//webpage += F("document.getElementById('fileType').innerHTML = 'Type: ' + file.type;\n");
webpage += F("client.onerror = function(e) {\n");
webpage += F("alert('onError'+e.type);\n");
webpage += F("};\n");
webpage += F("if (i == document.getElementById('fileA').files.length -1)");

webpage += F("client.onload = function(e) {\n");
webpage += F("document.getElementById('prozent').innerHTML = '100%';\n");

webpage += F("prog.value = prog.max;\n");
webpage += "window.location.href = '/dir?dir=" + parent + "';";
webpage += F("};\n");

webpage += F("else\n");

webpage += F("client.onload = function(e) {\n");
webpage += F("document.getElementById('prozent').innerHTML = '100%';\n");
//webpage += F("document.getElementById('fileName').innerHTML = 'Uploading: ' + file.name;\n");
webpage += F("prog.value = prog.max;\n");
webpage += F("};\n");

webpage += F("client.upload.onprogress = function(e) {\n");
webpage += F("var p = Math.round(100 / e.total * e.loaded);\n");
webpage += F("document.getElementById('progress').value = p;\n");
webpage += F("document.getElementById('prozent').innerHTML = p + '%';\n");
webpage += F("};\n");
webpage += F("client.onabort = function(e) {\n");
webpage += F("alert('Upload abgebrochen');\n");
webpage += F("};\n");
webpage += F("document.getElementById('fileName').innerHTML = 'Uploading: ' + file.name;\n");
webpage += F("client.open('POST', '/fupload');\n");
webpage += F("client.send(formData);\n");
webpage += F("}\n");
webpage += F("}\n");

webpage += F("</script>\n");
webpage += F("</html>\n");


    // append_page_footer();
    Serial.println("File upload stage-2");
    this->m_Server->send(200, "text/html", webpage);
    this->m_UploadCount = 0;
    this->m_WM.handleRequest();
}
String WebInterface::toUTF8(const String & in)
{
    String out = "";
    for (unsigned int i = 0; i < in.length(); i++)
    {
        if (in[i] < 128)
            out += in[i];
        else
        {
            out += char(0xc2+(in[i]>0xbf));
            out += char((in[i]&0x3f)+0x80);
        }
    }

    return out;
}
void WebInterface::Handle_File_Upload()
{
    //Serial.println("File upload stage-3");
    HTTPUpload &uploadfile = this->m_Server->upload(); // See https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WebServer/srcv
                                                       // For further information on 'status' structure, there are other reasons such as a failed transfer that could be used
    this->m_WM.handleRequest();
    //Serial.println(uploadfile.filename);
    //Serial.println(uploadfile.name);
    //Serial.println(uploadfile.type);
    int toUpload = this->m_Server->arg("fileCount").toInt();
    String parent = this->m_Server->arg("parent");
    //String filename = this->toUTF8(uploadfile.filename);
    String filename = uploadfile.filename;
    filename = parent + "/" + filename;
    if (uploadfile.status == UPLOAD_FILE_START)
    {
        Serial.println("File upload stage-4");

        Serial.printf("Upload File %d of %d Name: ", this->m_UploadCount, toUpload);
        Serial.println(filename);
        this->m_SDReader->Delete(filename); // Remove a previous version, otherwise data is appended the file again
        this->m_SDReader->OpenFile(filename);
        filename = String();

        this->m_LastUpdate = 0;
    }
    else if (uploadfile.status == UPLOAD_FILE_WRITE)
    {
        //Serial.println("File upload stage-5");
        this->m_SDReader->WriteFile(filename, uploadfile.buf, uploadfile.currentSize); // Write the received bytes to the file
        

        long total_size = this->m_Server->header("Content-Length").toInt();
        
        if (this->m_LastUpdate + 0.05*total_size < uploadfile.totalSize)
        {
            Serial.printf("%u of %u written\n", uploadfile.totalSize, total_size);
            
            this->m_LastUpdate = uploadfile.totalSize;
            /*
            append_page_header();
            webpage += "<label for='file'>Progess of upload of " + filename + " :</label>";
            int per = (100*uploadfile.totalSize)/total_size;
            webpage += "<progress id='file' value='" + String(per) + "' max='100'> " + String(per) + "%% </progress>";

            this->m_Server->send(200, "text/html", webpage);
            */
        }
    }
    else if (uploadfile.status == UPLOAD_FILE_END)
    {
        this->m_SDReader->CloseFile(filename); // Close the file again
        Serial.print("Upload Size: ");
        Serial.println(uploadfile.totalSize);
        for (int i = 0;i < uploadfile.filename.length();i++)
            Serial.printf("%02X", uploadfile.filename[i]);
        Serial.println("");
        this->m_UploadCount++;

        if (this->m_UploadCount == toUpload)
        {
            this->m_WM.handleRequest();
            Serial.println("Done uploading, should redirect");
            this->m_Server->sendHeader("Location", "/dir?dir=" + parent, true);
            this->m_Server->send(302, "text/plain", "");
        }
    }
    this->m_WM.handleRequest();
}

void WebInterface::File_Delete()
{
    this->m_WM.handleRequest();
    Serial.println("Enter");
    String parent = this->m_Server->arg("parent");
    for (int i = 0; i < this->m_Server->args(); i++)
    {

        Serial.print(this->m_Server->argName(i));
        Serial.println(this->m_Server->arg(i));
        if (this->m_Server->argName(i) != "parent")
        {
            String filename = this->m_Server->argName(i);
            filename.replace("%2F", "/");
            this->m_SDReader->Delete(filename);
        }
    }

    this->m_Server->sendHeader("Location", "/dir?dir=" + parent, true);
    this->m_Server->send(302, "text/plain", "");

    return;
    if (this->m_Server->args() > 0)
    { // Arguments were received
        if (this->m_Server->hasArg("delete"))
            this->m_SDReader->Delete(this->m_Server->arg(0));
    }
}

void WebInterface::List_Dir()
{
    this->m_WM.handleRequest();
    String dir = "/";
    int move = false;
    String moveFile = "";

    if (this->m_Server->args() > 0)
    { // Arguments were received
        if (this->m_Server->hasArg("dir"))
            dir = this->m_Server->arg(0);
        Serial.println(this->m_Server->arg(0));
        if (this->m_Server->hasArg("MoveUp") || this->m_Server->hasArg("MoveDown"))
        {
            moveFile = this->m_Server->arg(1);
            if (this->m_Server->hasArg("MoveUp"))
                move = 1;
            else
                move = 2;
        }
    }

    SendHTML_Header();
    webpage += F("<h3 class='rcorners_m'>SD Card Contents</h3><br>");
    webpage += F("<form action='/delete' method='get' id='selected'>");
    webpage += "<input type='hidden' name='parent' value='" + dir + "' />";
    webpage += F("<table align='center'>");
    webpage += F("<tr><th class=\"select\"></th><th class=\"order\">Order</th><th>Name/Type</th><th style='width:20%'>Type File/Dir</th><th>File Size</th></tr>");
    printDirectory(dir, move, moveFile);
    webpage += F("</table>");
    webpage += F("</form>");
    SendHTML_Content();

    append_page_footer(dir);
    SendHTML_Content();
    SendHTML_Stop(); // Stop is needed because no content length was sent
}

void WebInterface::printDirectory(String dirname, int move, String moveFile)
{
    this->m_WM.handleRequest();
    strVec dirs;
    fileVec files;

    this->m_SDReader->BrowseAndMove(dirname.c_str(), dirs, files, move, moveFile);

    if (dirname != "/")
    {
        String parent = "/";
        int pos = dirname.lastIndexOf('/');
        if (pos != 0)
        {
            parent = dirname.substring(0, pos);
        }
        webpage += "<tr><td></td><td></td><td>";
        webpage += "<a href='/dir?dir=" + parent + "'>";
        webpage += "^ Go one level up";
        webpage += "</a></td><td>Directory</td><td></td></tr>";
    }

    for (strVec::iterator file = dirs.begin(); file != dirs.end(); file++)
    {
        if (webpage.length() > 1000)
            SendHTML_Content();

        String full_path = dirname;
        if (full_path != "/")
            full_path += "/";
        full_path += *file;
        full_path.replace("/", "%2F");
        Serial.println(full_path);
        webpage += "<tr><td><input type='checkbox' name='" + full_path + "'></td><td></td><td>";
        webpage += "<a href='/dir?dir=" + full_path + "'>";
        webpage += *file;
        webpage += "</a></td><td>Directory</td><td></td></tr>";
    }

    for (fileVec::iterator file = files.begin(); file != files.end(); file++)
    {
        if (webpage.length() > 1000)
            SendHTML_Content();
            
        String full_path = dirname;
        if (full_path != "/")
            full_path += "/";

        full_path += file->name;
        full_path.replace("/", "%2F");
        webpage += "<tr><td><input type='checkbox' name='" + full_path + "'></td><td><a href='/dir?dir=" + dirname + "&&MoveDown=" + String(file->name) + "'>";
        webpage += "<i class=\"headerSortDown\"></i></a>";
        webpage += "<a href='/dir?dir=" + dirname + "&&MoveUp=" + String(file->name) + "'>";
        webpage += "<i class=\"headerSortUp\"></i></a></td><td>" + String(file->name) + "</td>";
        webpage += "<td>File</td>";
        int bytes = file->size;
        String fsize = "";
        if (bytes < 1024)
            fsize = String(bytes) + " B";
        else if (bytes < (1024 * 1024))
            fsize = String(bytes / 1024.0, 3) + " KB";
        else if (bytes < (1024 * 1024 * 1024))
            fsize = String(bytes / 1024.0 / 1024.0, 3) + " MB";
        else
            fsize = String(bytes / 1024.0 / 1024.0 / 1024.0, 3) + " GB";
        webpage += "<td>" + fsize + "</td></tr>";
    }
}

void WebInterface::Create_Dir()
{
    this->m_WM.handleRequest();
    Serial.println("Entering mkdir");
    String parent = this->m_Server->arg(0);
    bool failed = false;
    if (this->m_Server->hasArg("dir"))
    {
        String dir = this->m_Server->arg(1);

        if ((dir == "") || !(this->m_SDReader->MakeDir(parent + "/" + dir)))
        {
            Serial.print("Error creating directory");
            Serial.println(parent + "/" + dir);
        }
        this->m_Server->sendHeader("Location", "/dir?dir=" + parent, true);
        this->m_Server->send(302, "text/plain", "");
    }
    else
    {
        webpage += F("<html><body><script>");
        webpage += F("let text;");
        webpage += F("let dir = prompt(\"Please enter a folder name:\", \"NewFolder\");");
        webpage += "text = \"/mkdir?parent=" + parent + "&&dir=\" + dir;";
        webpage += F("window.location.replace(text);");
        webpage += F("</script></body></html>");
        this->m_Server->send(200, "text/html", webpage);
    }
}

void WebInterface::SendHTML_Header()
{
    this->m_Server->sendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    this->m_Server->sendHeader("Pragma", "no-cache");
    this->m_Server->sendHeader("Expires", "-1");
    this->m_Server->setContentLength(CONTENT_LENGTH_UNKNOWN);
    this->m_Server->send(200, "text/html", ""); // Empty content inhibits Content-length header so we have to close the socket ourselves.
    append_page_header();
    this->m_Server->sendContent(webpage);
    webpage = "";
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void WebInterface::SendHTML_Content()
{
    this->m_Server->sendContent(webpage);
    webpage = "";
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void WebInterface::SendHTML_Stop()
{
    this->m_Server->sendContent("");
    this->m_Server->client().stop(); // Stop is needed because no content length was sent
}

void WebInterface::LinkRFID()
{
    this->m_WM.handleRequest();
    String parent = this->m_Server->arg(0);
    Card currentCard = this->m_RFIDReader->GetCurrentCard();
    uint32_t id = currentCard.ID;

    append_page_header();

    String content = F("<FORM action='/link' method='post'>");
    content += "<input type='hidden' name='parent' value='" + parent + "' />";

    if (!this->m_Server->hasArg("mode"))
    {
        webpage += F("<h3>Place card to link to device and select playback mode</h3>");
        webpage += content;
        webpage += F("<select id='mode' name='mode'>");
        webpage += F("<option value='0'>Hörspiel Modus</option>");
        webpage += F("<option value='1'>Musik Modus</option>");
        webpage += F("<option value='2'>Party Modus</option>");
        webpage += F("</select>");
    }
    else
    {
        if ((this->m_Server->hasArg("overwrite")) || (currentCard.BlockData[CONFIG_BYTE] != IS_CONFIGURED)||((!currentCard.BlockData[WEBPORTAL_BYTE])&&(!RFIDCardManager::getInstance()->Configured(id))))
        {
            Serial.println("Writing data");
            if (currentCard.BlockData[CONFIG_BYTE] != IS_CONFIGURED)
            {
                currentCard.BlockData[CONFIG_BYTE] = IS_CONFIGURED;
            }
            currentCard.BlockData[MODE_BYTE] = this->m_Server->arg("mode").toInt();
            currentCard.BlockData[WEBPORTAL_BYTE] = 0;
            RFIDCardManager::getInstance()->SetCard(id, parent, currentCard.BlockData[MODE_BYTE]);
            this->m_RFIDReader->SetOutdata(currentCard.BlockData);
            webpage = "";

            this->m_Server->sendHeader("Location", "/dir?dir=" + parent, true);
            this->m_Server->send(302, "text/plain", "");

            return;
        }
        else
        {
            webpage += F("<h3>The current cared is configured ");

            if (currentCard.BlockData[WEBPORTAL_BYTE])
                webpage += F("as webportal access key");
            else
            {
                webpage += "and linked to " + RFIDCardManager::getInstance()->GetPath(id);
            }

            webpage += F(". Do you want to overwrite this?</h3>");
            webpage += content;
            webpage += "<input type='hidden' name='overwrite' value='1' />";
            webpage += "<input type='hidden' name='mode' value='" + this->m_Server->arg("mode") + "' />";
        }
    }
    webpage += F("<br><button class='buttons' style='width:10%' type='submit'>Configure tag</button><br></form>");
    webpage += "<a href='/dir?dir=" + parent + "'>[Back]</a><br><br>";

    this->m_Server->send(200, "text/html", webpage);
}

void WebInterface::CreateConfigRFID()
{
    this->m_WM.handleRequest();
    Card currentCard = this->m_RFIDReader->GetCurrentCard();
    Serial.println("Generate Admin tag");

    if (this->m_Server->hasArg("overwrite"))
    {
        currentCard.BlockData[CONFIG_BYTE] = IS_CONFIGURED;
        currentCard.BlockData[MODE_BYTE] = 0;
        currentCard.BlockData[WEBPORTAL_BYTE] = 1;

        this->m_RFIDReader->SetOutdata(currentCard.BlockData);

        this->m_Server->sendHeader("Location", "/", true);
        this->m_Server->send(302, "text/plain", "");
    }
    else
    {
        append_page_header();

        String content = F("<FORM action='/cfgrfid' method='post'>");

        if (currentCard.BlockData[CONFIG_BYTE] == IS_CONFIGURED)
        {
            webpage += F("<h3>The current cared is configured ");

            if (currentCard.BlockData[WEBPORTAL_BYTE])
                webpage += F("as webportal access key");
            else
            {
                uint32_t id = currentCard.ID;
                webpage += "and linked to " + RFIDCardManager::getInstance()->GetPath(id);
            }

            webpage += F(". Do you want to overwrite this?</h3>");
        }
        else
        {
            webpage += F("<h3>Do you want to use the current RFID tag for web portal access?</h3>");
        }
        webpage += content;
        webpage += "<input type='hidden' name='overwrite' value='1' />";
    }
    webpage += F("<br><button class='buttons' style='width:10%' type='submit'>Configure tag</button><br></form>");
    webpage += "<a href='/'>[Back]</a><br><br>";

    this->m_Server->send(200, "text/html", webpage);
}

void WebInterface::FactoryReset()
{
    Settings::getInstance()->Reset();
    this->m_WM.resetSettings();
    this->m_WM.reboot();
}

void WebInterface::Settings()
{
    if (this->m_Server->args() > 0)
    {
        bool ok = true;

        String hostname = this->m_Server->arg("hostname");
        String password = this->m_Server->arg("password");
        long timeout = this->m_Server->arg("idletimeout").toInt();
        long maxvolume = this->m_Server->arg("maxvolume").toInt();
        long maxvolumehp = this->m_Server->arg("maxvolumehp").toInt();

        if ((hostname.length() <= 0)||(hostname.length() > 63))
            ok = false;
        else if (hostname[0] == '-')
            ok = false;
        else
        {
            for (unsigned int i = 0; i < hostname.length();i++)
            {
                if ((hostname[i] >= 'a')&&(hostname[i] <= 'z'))
                    continue;
                else if ((hostname[i] >= 'A')&&(hostname[i] <= 'Z'))
                    continue;
                else if ((hostname[i] >= '0')&&(hostname[i] <= '9'))
                    continue;
                else if (hostname[i] == '-')
                    continue;
                else
                {
                    ok = false;
                    break;
                }
            }
        }

        String target = "/";

        if (!ok)
        {
            target = "/settings";
        }
        else
        {
            Settings::getInstance()->SetHostname(hostname);
            Settings::getInstance()->SetPassword(password);
            Settings::getInstance()->SetTimeoutPowerDown(timeout);
            Settings::getInstance()->SetMaxVolume(maxvolume);
            Settings::getInstance()->SetMaxVolumeHeadphones(maxvolumehp);

            Settings::getInstance()->SaveSettings();
        }

        this->m_Server->sendHeader("Location", target, true);
        this->m_Server->send(302, "text/plain", "");
    }
    else
    {
        append_page_header();
        String parent = this->m_Server->arg(0);
        webpage += F("<h3>Settings</h3>");
        webpage += F("<FORM action='/settings' method='post'>");
        webpage += F("<label for='hostname'>Hostname:</label><br>");
        webpage += "<input type='text' id='hostname' name='hostname' value='" + Settings::getInstance()->GetHostname() + "'><br>";
        webpage += F("<label for='password'>Password:</label><br>");
        webpage += "<input type='text' id='password' name='password' value='" + Settings::getInstance()->GetPassword() + "'><br>";
        webpage += F("<label for='idletimeout'>Seconds to wait before shutdown when idle (not implemented):</label><br>");
        webpage += "<input type='number' id='idletimeout' name='idletimeout' min='30' max='600' value='" + String( Settings::getInstance()->GetTimeoutPowerDown()) + "'><br>";
        webpage += F("<label for='maxvolume'>Maximum volume (speaker):</label><br>");
        webpage += "<input type='number' id='maxvolume' name='maxvolume' min='10' max='50' value='" + String( Settings::getInstance()->GetMaxVolume()) + "'><br>";
        webpage += F("<label for='maxvolumehp'>Maximum volume (headdphones):</label><br>");
        webpage += "<input type='number' id='maxvolumehp' name='maxvolumehp' min='10' max='50' value='" + String( Settings::getInstance()->GetMaxVolumeHeadphone()) + "'><br>";
        webpage += F("<br><button class='buttons' style='width:10%' type='submit'>Save</button><br></form>");
        webpage += "<a href='/dir?dir=" + parent + "'>[Back]</a><br><br>";
        webpage += "<script>";
        webpage += "function onChange() {";
        webpage += "var numFiles = document.getElementById('fupload').files.length;";
        webpage += "document.getElementById(\"fileCount\").value = numFiles;";
        webpage += "}";
        webpage += "</script>";
        // append_page_footer();
        Serial.println("File upload stage-2");
        this->m_Server->send(200, "text/html", webpage);
        this->m_UploadCount = 0;
        this->m_WM.handleRequest();
    }
}

Settings *Settings::m_Instance = 0;

void Settings::SaveSettings()
{
    this->m_Preferences.begin("SophiBox", false);

    this->m_Preferences.putString("Hostname", this->m_Hostname);
    this->m_Preferences.putString("Password", this->m_Password);
    this->m_Preferences.putUShort("TimeoutPD", this->m_TimeoutPowerDown);
    this->m_Preferences.putUChar("MaxVolume", this->m_MaxVolume);
    Serial.printf("Saving volume %d\n", this->m_MaxVolume);
    this->m_Preferences.putUChar("MaxVolumeHP", this->m_MaxVolumeHeadphones);

    this->m_Preferences.end();
}

void Settings::LoadSettings()
{
    this->m_Preferences.begin("SophiBox", true);

    this->m_Hostname = this->m_Preferences.getString("Hostname", DEFAULT_HOSTNAME);
    this->m_Password = this->m_Preferences.getString("Password", DEFAULT_PASSWORD);
    this->m_TimeoutPowerDown = this->m_Preferences.getUShort("TimeoutPD", DEFAULT_POWER_TIMEOUT);
    this->m_MaxVolume = this->m_Preferences.getUChar("MaxVolume", DEFAULT_MAX_VOL);
    Serial.printf("Loading volume %d\n", this->m_MaxVolume);
    this->m_MaxVolumeHeadphones = this->m_Preferences.getUChar("MaxVolumeHP", DEFAULT_MAX_VOL_HP);

    this->m_Preferences.end();
}

void Settings::Reset()
{
    this->m_Preferences.begin("SophiBox", false);
    this->m_Preferences.clear();
    this->m_Preferences.end();
}

Settings::Settings()
{
    this->LoadSettings();
}

Settings* Settings::getInstance()
{
    if (m_Instance == 0)
    {
        m_Instance = new Settings;
    }
    return m_Instance;
}
