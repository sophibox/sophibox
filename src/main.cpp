#include <Arduino.h>
#include "RFID.h"
#include "SDCard.h"
#include <SPI.h>
#include "defines.h"
#include "WebInterface.h"
#include "AudioPlayer.h"
#include "Preferences.h"
#include "LEDs.h"

//#include "Power.h"

RFIDCardReader *rfidReader;
SDCardReader *sdReader;
WebInterface *webInterface;
AudioPlayer *audioPlayer;
Preferences pref;
//PowerManagement power(CHARGER_PIN, BAT_STATE_PIN, BAT_QUERY_PIN, SHUTDOWN_PIN);

bool ConfigPortalOpen = false;
unsigned long last_RFID = 0;
unsigned long t1_start = 0;
unsigned long t2_start = 0;
bool t1_active = false;
bool t2_active = false;
int last_volume;
unsigned long last_action = 0;

uint8_t initBrightLeft = 50;
uint8_t initBrightRight = 30;

LED ledLeft(0, LED1_PIN, initBrightLeft);
LED ledRight(1, LED2_PIN, initBrightRight);

std::vector<LightMode> modesLeft;
std::vector<LightMode> modesRight;

enum LightStates {OFF, BOOTING, FAST_FORWARD, REWIND, PLAYING, SETTINGS};

LightStates lastMode;

void updateFromFS(fs::FS &fs);

void UpdateLEDs(LightStates state)
{
    if (state == lastMode)
        return;
    lastMode = state;
    ledLeft.SetLightMode(modesLeft[state]);
    ledRight.SetLightMode(modesRight[state]);
}

void setup()
{
    //OFF
    modesLeft.push_back({0, 0});
    modesRight.push_back({0, 0});
    //READY
    modesLeft.push_back({initBrightLeft, 0});
    modesRight.push_back({initBrightRight, 0});
    //FASTFORWAD
    modesLeft.push_back({0, 0});
    modesRight.push_back({initBrightRight, 200});
    //REWIND
    modesLeft.push_back({initBrightLeft, 200});
    modesRight.push_back({0, 0});
    //Playing
    modesLeft.push_back({initBrightLeft, 0});
    modesRight.push_back({initBrightRight, 500});
    //SETTINGS
    modesLeft.push_back({initBrightLeft, 200});
    modesRight.push_back({initBrightRight, 200});

    UpdateLEDs(BOOTING);

    last_volume = 0;
    // put your setup code here, to run once:
    //pref.begin("SophiBox", false);
    //pref.clear();
    Serial.begin(115200);
    SPI.begin();
    delay(100);

    rfidReader = new RFIDCardReader(MFRC_SS_PIN, MFRC_RST_PIN);
    delay(100);
    sdReader = new SDCardReader(SD_SS_PIN);
    delay(100);
    //updateFromFS(SD);
    delay(100);
    webInterface = new WebInterface(sdReader, rfidReader);
    delay(100);
    Serial.println("Before Audio");
    audioPlayer = new AudioPlayer(I2S_BCLK_PIN, I2S_LRC_PIN, I2S_DOUT_PIN, sdReader);
    Serial.println("After Audio");
    delay(100);
}

void loop()
{
    // put your main code here, to run repeatedly:
    Card card = rfidReader->GetCurrentCard();
    // WebSerial.println(cardID.ID);
    // Serial.println(card.ID);
    // Serial.println(NO_CARD_ID);
    // for (int i = 0;i < 16;i++)
    //   Serial.print(card.BlockData[i]);
    // Serial.println();
    bool configured = RFIDCardManager::getInstance()->Configured(card.ID);
    if ((card.ID != NO_CARD_ID) && (card.changed))
    {
        if (((card.BlockData[WEBPORTAL_BYTE]) || (!configured)) && (!ConfigPortalOpen))
        { // Card with webportal set or an configured card
            Serial.println("Config card found! Opening portal!");
            ConfigPortalOpen = true;
            webInterface->ActivateInterface();

            UpdateLEDs(SETTINGS);
        }
        else if (!ConfigPortalOpen)
        {
            // Play music! ;)
            audioPlayer->SetCard(card);
            UpdateLEDs(PLAYING);
        }
    }
    else if ((card.ID == NO_CARD_ID) && (card.changed))
    {
        if (audioPlayer->IsPlaying())
        {
            Serial.println("Card removed. Stopping play");
            audioPlayer->Stop();
            UpdateLEDs(OFF);
        }
    }
    if (ConfigPortalOpen)
    {
        bool active = webInterface->Loop();

        //last_action = millis();

        if (!active)
        {
            ConfigPortalOpen = false;
            webInterface->DeactivateInterface();
            Serial.println("Deactivated webportal");
            UpdateLEDs(OFF);
        }
    }
    audioPlayer->Loop();

    ledLeft.Update();
    ledRight.Update();

    unsigned long curTime = millis();

    //if (curTime - last_action > IDLE_TIME_OUT)
    //{
    //    Serial.println("Shutting down");
    //    power.Shutdown();
    //}

    //if (audioPlayer->IsPlaying())
    //    last_action = curTime;

    if (curTime - last_RFID > RFID_WAIT)
    {
        last_RFID = curTime;
        
        rfidReader->DataExchange();

        if (ConfigPortalOpen)
            return;

        int headphones = digitalRead(HEADPHONES_PIN);
        //Serial.printf("Headphones %d\n", headphones);

        int maxVol = 0; 
        if (headphones == 0)
            maxVol = Settings::getInstance()->GetMaxVolumeHeadphone();
        else
            maxVol = Settings::getInstance()->GetMaxVolume();

        int analogValue = analogRead(VOLUME_PIN);
        int volume = (maxVol * analogValue) / 4095;

        volume = maxVol - volume;

        //if (volume > maxVol)
        //    volume = maxVol;

        Serial.printf("vol %d max vol %d\n", volume, maxVol);


        if ((abs(volume-last_volume) > 2)||(volume == 0)||(volume == maxVol))
        {
            audioPlayer->SetVolume(volume);
            last_volume = volume;
        }       
        
        int t1 = touchRead(TOUCH1_PIN);
        int t2 = touchRead(TOUCH2_PIN);

        //Serial.printf("T1 %d T2 %d\n", t1, t2);

        if ((t1 < TOUCH_THRS)&&(t2 < TOUCH_THRS))
        {
            audioPlayer->Restart();
            t1_active = false;
            t2_active = false;
            Serial.println("Reset");

            delay(1000);
        }
        else
        {
            if (t1 < TOUCH_THRS)
            {
                if (!t1_active)
                {
                    t1_active = true;
                    t1_start = curTime;
                }
                if (curTime - t1_start > HOLD_THRS)
                {
                    audioPlayer->FastForward();
                    UpdateLEDs(FAST_FORWARD);
                }
            }
            else if (t1_active)
            {
                t1_active = false;
                if (curTime - t1_start <= HOLD_THRS)
                    audioPlayer->Next(true);
                    Serial.println("Next");
            }

            if (t2 < TOUCH_THRS)
            {
                if (!t2_active)
                {
                    t2_active = true;
                    t2_start = curTime;
                }
                if (curTime - t2_start > HOLD_THRS)
                {
                    audioPlayer->Rewind();
                    UpdateLEDs(REWIND);
                }
            }
            else if (t2_active)
            {
                t2_active = false;
                if (curTime - t2_start <= HOLD_THRS)
                    audioPlayer->Back();
                    Serial.println("Back");
            }
        }

        if ((!t1_active)&&(!t2_active))
        {
            if (audioPlayer->IsPlaying())
                UpdateLEDs(PLAYING);
            else
                UpdateLEDs(OFF);
        }
            
        
    }
    // delay(200);
}

void audio_eof_mp3(const char *file)
{
    audioPlayer->Next();
}