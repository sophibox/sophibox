#include "Power.h"

PowerManagement::PowerManagement(uint8_t charge_pin, uint8_t bat_state_pin, uint8_t bat_query_pin, uint8_t shutdown_pin)
{
    this->m_ChargePin = charge_pin;
    this->m_BatStatePin = bat_state_pin;
    this->m_BatQueryPin = bat_query_pin;
    this->m_ShutdownPin = shutdown_pin;

    pinMode(this->m_ShutdownPin, OUTPUT);
    digitalWrite(this->m_ShutdownPin, HIGH);

    pinMode(this->m_BatQueryPin, OUTPUT);
    digitalWrite(this->m_BatQueryPin, LOW);
}

bool PowerManagement::IsCharging()
{
    return digitalRead(this->m_ChargePin) > 0;
}

uint8_t PowerManagement::GetBatteryLevel()
{
    digitalWrite(this->m_BatQueryPin, HIGH);
    delay(10);
    int adc = analogRead(this->m_BatStatePin);
    
    digitalWrite(this->m_BatQueryPin, LOW);
    int battery_percentage = 100 * (adc - BATTERY_MIN_ADC) / (BATTERY_MAX_ADC - BATTERY_MIN_ADC);

    if (battery_percentage < 0)
        battery_percentage = 0;
    if (battery_percentage > 100)
        battery_percentage = 100;

    return battery_percentage;
}

void PowerManagement::Shutdown()
{
    digitalWrite(this->m_ShutdownPin, LOW);
    delay(5000);
    digitalWrite(this->m_ShutdownPin, HIGH);
    delay(5000);
    digitalWrite(this->m_ShutdownPin, LOW);
    delay(5000);
    digitalWrite(this->m_ShutdownPin, HIGH);
    delay(5000);
    digitalWrite(this->m_ShutdownPin, LOW);
}
