#include "RFID.h"

RFIDCardManager *RFIDCardManager::m_Instance = 0;

RFIDCardManager::RFIDCardManager()
{
    this->m_Preferences.begin("SophiBox", true);

    uint32_t numCards = this->m_Preferences.getUInt("NumCards");
    Serial.printf("Got %u cards saved\n", numCards);

    for (uint32_t i = 0; i < numCards; i++)
    {
        MultimediaRFID mmCard;
        mmCard.num = i;
        char sname[32];

        sprintf(sname, "%04u_path", i);
        mmCard.path = this->m_Preferences.getString(sname, "");
        sprintf(sname, "%04u_mode", i);
        mmCard.mode = this->m_Preferences.getUChar(sname, 0);
        sprintf(sname, "%04u_file", i);
        mmCard.c_file = this->m_Preferences.getUShort(sname, 0);
        sprintf(sname, "%04u_pos", i);
        mmCard.c_pos = this->m_Preferences.getULong(sname, 0);
        Serial.println(mmCard.path);
        sprintf(sname, "%04u_id", i);
        mmCard.ID = this->m_Preferences.getULong(sname, 0);
        Serial.println(mmCard.ID);
        mmCard.lastseen = 0;

        this->m_RFIDCollection[mmCard.ID] = mmCard;
    }

    this->m_Preferences.end();
}

void RFIDCardManager::SaveCard(uint32_t id)
{
    char sname[32];
    Serial.printf("Saving %u\n", this->m_RFIDCollection[id].num);
    this->m_Preferences.begin("SophiBox", false);
    sprintf(sname, "%04u_path", this->m_RFIDCollection[id].num);
    this->m_Preferences.putString(sname, this->m_RFIDCollection[id].path);
    sprintf(sname, "%04u_mode", this->m_RFIDCollection[id].num);
    this->m_Preferences.putUChar(sname, this->m_RFIDCollection[id].mode);
    sprintf(sname, "%04u_file", this->m_RFIDCollection[id].num);
    this->m_Preferences.putUShort(sname, this->m_RFIDCollection[id].c_file);
    sprintf(sname, "%04u_pos", this->m_RFIDCollection[id].num);
    this->m_Preferences.putULong(sname, this->m_RFIDCollection[id].c_pos);
    sprintf(sname, "%04u_id", this->m_RFIDCollection[id].num);
    this->m_Preferences.putULong(sname, this->m_RFIDCollection[id].ID);
    this->m_Preferences.end();
}

RFIDCardManager *RFIDCardManager::getInstance()
{
    if (m_Instance == 0)
    {
        m_Instance = new RFIDCardManager;
    }
    return m_Instance;
}

void RFIDCardManager::cleanInstance()
{
    delete m_Instance;
    m_Instance = 0;
}

uint32_t RFIDCardManager::SetCard(uint32_t id, String path, uint8_t mode)
{
    MultimediaRFID mmCard;
    mmCard.mode = mode;
    mmCard.path = path;
    mmCard.c_file = 0;
    mmCard.c_pos = 0;
    mmCard.ID = id;
    Serial.printf("Setting card %u\n", id);

    if (!this->Configured(id))
    {
        mmCard.num = this->m_RFIDCollection.size();
    }
    else
    {
        mmCard.num = this->m_RFIDCollection[id].num;
    }

    this->m_RFIDCollection[id] = mmCard;

    this->m_Preferences.begin("SophiBox", false);
    this->m_Preferences.putUInt("NumCards", this->m_RFIDCollection.size());
    this->m_Preferences.end();

    // Serial.print("Created card with id ");
    // Serial.print(id);
    // Serial.print(mode);
    // Serial.println(path);
    this->SaveCard(id);

    return id;
}

String RFIDCardManager::GetPath(uint32_t id)
{
    String path;

    if (this->m_RFIDCollection.find(id) != this->m_RFIDCollection.end())
        path = this->m_RFIDCollection[id].path;

    return path;
}

uint8_t RFIDCardManager::GetMode(uint32_t id)
{
    uint8_t mode;

    if (this->m_RFIDCollection.find(id) != this->m_RFIDCollection.end())
        mode = this->m_RFIDCollection[id].mode;

    return mode;
}

void RFIDCardManager::GetPosition(uint32_t id, uint16_t &file, uint32_t &pos)
{
    if (this->m_RFIDCollection.find(id) != this->m_RFIDCollection.end())
    {
        Serial.printf("Current time %lu Last seen %lu\n", millis(), this->m_RFIDCollection[id].lastseen);
        if (((this->m_RFIDCollection[id].lastseen > 0)&&(millis() - this->m_RFIDCollection[id].lastseen < SAVE_TIME_OUT))||(this->m_RFIDCollection[id].mode == 0))
        {
            file = this->m_RFIDCollection[id].c_file;
            pos = this->m_RFIDCollection[id].c_pos;
        }
        else
        {
            if (this->m_RFIDCollection[id].mode == 2)
                file = 65535;
            else
                file = 0;
            pos = 0;
        }
    }
}

void RFIDCardManager::SetPosition(uint32_t id, uint16_t file, uint32_t pos)
{
    if (this->m_RFIDCollection.find(id) != this->m_RFIDCollection.end())
    {
        this->m_RFIDCollection[id].c_file = file;
        this->m_RFIDCollection[id].c_pos = pos;
        this->m_RFIDCollection[id].lastseen = millis();

        char sname[32];
        this->m_Preferences.begin("SophiBox", false);
        sprintf(sname, "%04u_file", this->m_RFIDCollection[id].num);
        this->m_Preferences.putUShort(sname, file);
        sprintf(sname, "%04u_pos", this->m_RFIDCollection[id].num);
        this->m_Preferences.putULong(sname, pos);
        this->m_Preferences.end();
    }
}

bool RFIDCardManager::Configured(uint32_t id)
{
    return this->m_RFIDCollection.find(id) != this->m_RFIDCollection.end();
}

RFIDCardReader::~RFIDCardReader()
{
    this->m_ThreadRunning = false;
    vTaskDelete(this->m_ThreadHandle);
    delete this->m_MFRC522;
    vSemaphoreDelete(this->m_Mutex);
}

Card RFIDCardReader::GetCurrentCard()
{
    xSemaphoreTake(this->m_Mutex, portMAX_DELAY); // enter critical section
    Card tmp = this->m_CurrentCard;
    this->m_CurrentCard.changed = false;
    xSemaphoreGive(this->m_Mutex); // exit critical section
    return tmp;
}

void RFIDCardReader::SetOutdata(const byte *data)
{
    xSemaphoreTake(this->m_Mutex, portMAX_DELAY); // enter critical section
    for (int i = 0; i < 16; i++)
        this->m_BlockData[i] = data[i];

    m_GotOutData = true;
    xSemaphoreGive(this->m_Mutex); // exit critical section
}

void RFIDCardReader::ThreadFunction(void *pvParameters)
{
    RFIDCardReader *parent = (RFIDCardReader *)pvParameters;

    while (parent->m_ThreadRunning)
    {
        xSemaphoreTake(SPIMutex, portMAX_DELAY);
        parent->DataExchange();
        xSemaphoreGive(SPIMutex);
        delay(RFID_WAIT);
    }
}
bool RFIDCardReader::PICC_IsCardPresent()
{
    byte bufferATQA[2];
    byte bufferSize = sizeof(bufferATQA);

    // Reset baud rates
    this->m_MFRC522->PCD_WriteRegister(MFRC522::TxModeReg, 0x00);
    this->m_MFRC522->PCD_WriteRegister(MFRC522::RxModeReg, 0x00);
    // Reset ModWidthReg
    this->m_MFRC522->PCD_WriteRegister(MFRC522::ModWidthReg, 0x26);

    MFRC522::StatusCode result = this->m_MFRC522->PICC_WakeupA(bufferATQA, &bufferSize);

    return (result == MFRC522::STATUS_OK || result == MFRC522::STATUS_COLLISION);
} // End PICC_IsNewCardPresent()

void RFIDCardReader::DataExchange()
{
    bool success = true;
    bool updated = false;
    bool cardPresent = true;
    byte BlockData[18];

    if (!this->m_MFRC522->PICC_IsNewCardPresent())
    {
        success = false;
        if (!this->PICC_IsCardPresent())
        {
            cardPresent = false;
        }
    }

    if (success||cardPresent)
        this->m_NumTries = 0;

    if (success || (cardPresent && (this->m_GotOutData||(this->m_CurrentCard.ID == NO_CARD_ID))))
    {
        Serial.println("New Card present");
        success = true;
        if (!this->m_MFRC522->PICC_ReadCardSerial())
        {
            Serial.println("Can't read serial");
            success = false;
        }
    }

    uint32_t cardId = 0;

    if (!success && !cardPresent)
    {
        if (this->m_NumTries < NUM_TRIES)
        {
            Serial.printf("No card add try %d\n", this->m_NumTries);
            this->m_NumTries++;
        }
        else
        {
            cardId = NO_CARD_ID;
            Serial.println("No card");

            for (int i = 0; i < 16; i++)
                BlockData[i] = 0;
        }
    }
    else if (success && cardPresent)
    {
        // Show UID on serial monitor
        Serial.print("UID tag :");
        // String content= "";
        // byte letter;

        for (byte i = 0; i < this->m_MFRC522->uid.size; i++)
        {
            if (i < 4)
            {
                cardId = cardId << 8;
                cardId += this->m_MFRC522->uid.uidByte[i];
            }

            Serial.print(this->m_MFRC522->uid.uidByte[i] < 0x10 ? " 0" : " ");
            Serial.print(this->m_MFRC522->uid.uidByte[i], HEX);
            // content.concat(String(this->m_MFRC522->uid.uidByte[i] < 0x10 ? " 0" : " "));
            // content.concat(String(this->m_MFRC522->uid.uidByte[i], HEX));
        }
        Serial.println();

        xSemaphoreTake(this->m_Mutex, portMAX_DELAY); // enter critical section
        if ((cardId != this->m_CurrentCard.ID) || (this->m_GotOutData))
        {
            updated = true;
            MFRC522::MIFARE_Key key;

            for (byte i = 0; i < 6; i++)
            {
                key.keyByte[i] = 0xFF;
            }

            MFRC522::PICC_Type piccType = this->m_MFRC522->PICC_GetType(this->m_MFRC522->uid.sak);

            if (this->m_GotOutData)
            {
                WriteDataToBlock(BLOCK_ID, this->m_BlockData, key);

                for (int i = 0; i < 16; i++)
                    BlockData[i] = this->m_BlockData[i];

                this->m_GotOutData = false;
            }
            else
            {
                ReadDataFromBlock(BLOCK_ID, BlockData, key);
            }
            this->m_NumTries = 0;
            // instructs the PICC when in the ACTIVE state to go to a "STOP" state
            this->m_MFRC522->PICC_HaltA();
            // "stop" the encryption of the PCD, it must be called after communication with authentication, otherwise new communications can not be initiated
            this->m_MFRC522->PCD_StopCrypto1();
        }
        xSemaphoreGive(this->m_Mutex); // exit critical section
    }

    xSemaphoreTake(this->m_Mutex, portMAX_DELAY); // enter critical section
    if (((cardId > 0) && (cardId != this->m_CurrentCard.ID)) || (updated))
    {
        this->m_CurrentCard.ID = cardId;
        this->m_CurrentCard.changed = true;

        for (int i = 0; i < 16; i++)
        {
            this->m_CurrentCard.BlockData[i] = BlockData[i];
        }
    }

    xSemaphoreGive(this->m_Mutex); // exit critical section
}

RFIDCardReader::RFIDCardReader(uint8_t chipSelectPin, uint8_t resetPowerDownPin)
{
    xSemaphoreTake(SPIMutex, portMAX_DELAY);
    this->m_MFRC522 = new MFRC522(chipSelectPin, resetPowerDownPin);
    this->m_MFRC522->PCD_Init();
    xSemaphoreGive(SPIMutex);

    this->m_ThreadRunning = true;
    /*
    BaseType_t xReturned;
    xReturned = xTaskCreate(
                    RFIDCardReader::ThreadFunction,
                    "RFIDReader",
                    STACK_SIZE,
                    ( void * ) this,
                    tskIDLE_PRIORITY,
                    &(this->m_ThreadHandle));
    */
    this->m_Mutex = xSemaphoreCreateMutex();
    assert(this->m_Mutex);

    this->m_CurrentCard = NO_CARD;
    this->m_NumTries = 0;

    m_GotOutData = false;
}

void RFIDCardReader::WriteDataToBlock(int blockNum, byte blockData[], MFRC522::MIFARE_Key &key)
{
    /* Authenticating the desired data block for write access using Key A */
    MFRC522::StatusCode status = this->m_MFRC522->PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, blockNum, &key, &(this->m_MFRC522->uid));
    if (status != MFRC522::STATUS_OK)
    {
        Serial.print("Authentication failed for Write: ");
        Serial.println(this->m_MFRC522->GetStatusCodeName(status));
        return;
    }
    else
    {
        Serial.println("Authentication success");
    }

    /* Write data to the block */
    status = this->m_MFRC522->MIFARE_Write(blockNum, blockData, 16);
    if (status != MFRC522::STATUS_OK)
    {
        Serial.print("Writing to Block failed: ");
        Serial.println(this->m_MFRC522->GetStatusCodeName(status));
        return;
    }
    else
    {
        Serial.println("Data was written into Block successfully");
    }
}

void RFIDCardReader::ReadDataFromBlock(int blockNum, byte readBlockData[], MFRC522::MIFARE_Key &key)
{
    /* Authenticating the desired data block for Read access using Key A */
    byte bufferLen = 18;
    MFRC522::StatusCode status = this->m_MFRC522->PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, blockNum, &key, &(this->m_MFRC522->uid));

    if (status != MFRC522::STATUS_OK)
    {
        Serial.print("Authentication failed for Read: ");
        Serial.println(this->m_MFRC522->GetStatusCodeName(status));
        return;
    }
    else
    {
        Serial.println("Authentication success");
    }

    /* Reading data from the Block */
    status = this->m_MFRC522->MIFARE_Read(blockNum, readBlockData, &bufferLen);
    if (status != MFRC522::STATUS_OK)
    {
        Serial.print("Reading failed: ");
        Serial.println(this->m_MFRC522->GetStatusCodeName(status));
        return;
    }
    else
    {
        Serial.println("Block was read successfully");
    }
}