#ifndef WEBINTERFACE_H
#define WEBINTERFACE_H

#include <Preferences.h>
#include "CSS.h"
#include "SDCard.h"
#include "RFID.h"
#include <WiFiManager.h> // https://github.com/tzapu/WiFiManager

#ifdef ESP8266
  #include <ESP8266WiFi.h>       // Built-in
  #include <ESP8266WebServer.h>  // Built-in
  using WM_WebServer = ESP8266WebServer;
#else
  #include <WiFi.h>              // Built-in
  #include <WebServer.h>
  using WM_WebServer = WebServer;
#endif

class Settings
{
    public:
        static Settings* getInstance();
        void SaveSettings();
        void LoadSettings();
        void Reset();

        String GetHostname() {return this->m_Hostname;}
        String GetPassword() {return this->m_Password;}
        uint16_t GetTimeoutPowerDown() {return this->m_TimeoutPowerDown;}
        uint8_t GetMaxVolume() {return this->m_MaxVolume;}
        uint8_t GetMaxVolumeHeadphone() {return this->m_MaxVolumeHeadphones;}
        void SetHostname(const String & val){this->m_Hostname = val;}
        void SetPassword(const String & val){this->m_Password = val;}
        void SetTimeoutPowerDown(const uint16_t & val){this->m_TimeoutPowerDown = val;}
        void SetMaxVolume(const uint8_t & val){this->m_MaxVolume = val;}
        void SetMaxVolumeHeadphones(const uint8_t & val){this->m_MaxVolumeHeadphones = val;}
    private:
        Settings();
        Settings(Settings& other) {};
        Settings& operator=(Settings& other) {return other;};

        static Settings* m_Instance;
        Preferences m_Preferences;

        String m_Hostname;
        String m_Password;
        uint16_t m_TimeoutPowerDown;
        uint8_t m_MaxVolume;
        uint8_t m_MaxVolumeHeadphones;
};

class WebInterface
{
    public:
        WebInterface(SDCardReader* sdReader, RFIDCardReader* rfidReader);
        ~WebInterface();
        void ActivateInterface();
        void DeactivateInterface();
        bool Loop();
    private:
        String toUTF8(const String & in);
        void File_Upload();
        void Handle_File_Upload();
        void File_Delete();
        void List_Dir();
        void Create_Dir();
        void printDirectory(String dirname, int move, String moveFile);
        void SendHTML_Header();
        void SendHTML_Content();
        void SendHTML_Stop();
        void LinkRFID();
        void CreateConfigRFID();
        void FactoryReset();
        void Settings();

        std::function<void()> m_PFileUpload;
        std::function<void()> m_PHandleFileUpload;
        std::function<void()> m_PHandleFileUpload2;
        std::function<void()> m_PFileDelete;
        std::function<void()> m_PListDir;
        std::function<void()> m_PCreateDir;
        std::function<void()> m_PLinkRFID;
        std::function<void()> m_CreateConfigRFID;
        std::function<void()> m_PSettings;
        std::function<void()> m_PFReset;

        SDCardReader* m_SDReader;
        RFIDCardReader* m_RFIDReader;
        WM_WebServer* m_Server;
        WiFiManager m_WM;

        int m_UploadCount;
        unsigned long m_Timeout;

        size_t m_LastUpdate;
};

#endif /* WEBINTERFACE_H */
