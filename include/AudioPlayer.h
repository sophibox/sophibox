#ifndef AUDIOPLAYER_H
#define AUDIOPLAYER_H

#include "defines.h"
#include "Audio.h"
#include "SDCard.h"
#include "RFID.h"
#include <vector>

class AudioPlayer
{
    public:
        AudioPlayer(uint8_t BCLK, uint8_t LRC, uint8_t DOUT, SDCardReader* sdReader);
        ~AudioPlayer();
        void SetCard(Card card);
        void SetVolume(uint8_t volume);
        void Restart();
        void Stop();
        void Back();
        void Next(bool manual = false);
        void Rewind();
        void FastForward();
        bool IsPlaying();
        void Loop();
    private:
        void SetPlaying(bool play);
        Audio m_Audio;
        SDCardReader* m_SDReader;
        void SaveState();
        static void ThreadFunction(void* pvParameters);
        void* m_ThreadHandle;
        bool m_ThreadRunning;
        SemaphoreHandle_t m_Mutex;
        strVec m_Files;
        RFIDCardManager* m_CardManager;
        uint32_t m_CardID;
        uint16_t m_CurrentTrack;
        uint8_t m_CurrentMode;
        bool m_Playing;
        unsigned long m_LastSave;
};

#endif /* AUDIO_H */
