#ifndef SDCARD_H
#define SDCARD_H

#include "defines.h"
#include <FS.h>

class SDCardReader
{
    public:
        SDCardReader(uint8_t chipSelectPin);
        ~SDCardReader();
        void Delete(const String & path);
        void BrowseAndMove(const char * dirname, strVec & dirs, fileVec & files, int move = 0, String moveFile = "");
        bool OpenFile(const String & path);
        bool WriteFile(const String & path, const uint8_t* data, size_t size);
        bool CloseFile(const String & path);
        bool MakeDir(const String & path);
    private:
        bool IsOpened(const String & path);
        uint8_t m_CardType;
        uint64_t m_CardSize;
        bool m_SDPresent;
        void DeleteRecursive(const String & path);
        std::map<String, File> m_OpenFiles;
};

#endif /* SDCARD_H */
