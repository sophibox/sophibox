#ifndef LEDS_H
#define LEDS_H

#include "defines.h"

struct LightMode
{
    uint8_t brightness;
    unsigned long blinking;
};

class LED
{
    public:
        LED(uint8_t channel, uint8_t pin,uint8_t init_brightness=0, uint32_t frequency=5000);
        ~LED();
        void SetBrightness(uint8_t brightness);
        uint8_t GetBrightness();
        void SetBlinking(unsigned long wait);
        void SetLightMode(const LightMode & mode);
        void Update();
    private:
        uint8_t m_Brightness;
        uint8_t m_CurrentBrightness;
        uint8_t m_Channel;
        uint8_t m_Pin;
        unsigned long m_BlinkWait;
        unsigned long m_LastBlink;

};

#endif /* LEDS_H */
