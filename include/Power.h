#ifndef POWER_H
#define POWER_H

#include "defines.h"

class PowerManagement
{
    public:
        PowerManagement(uint8_t charge_pin, uint8_t bat_state_pin, uint8_t bat_query_pin, uint8_t shutdown_pin);
        bool IsCharging();
        uint8_t GetBatteryLevel();
        void Shutdown();
    private:
        uint8_t m_ChargePin;
        uint8_t m_BatStatePin;
        uint8_t m_BatQueryPin;
        uint8_t m_ShutdownPin;
};

#endif /* POWER_H */
