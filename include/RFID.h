#ifndef RFID_H
#define RFID_H

#include "defines.h"
#include <esp32-hal.h>
#include <MFRC522.h>
#include <Preferences.h>

class RFIDCardManager
{
    public:
        static RFIDCardManager* getInstance();
        static void cleanInstance();
        uint32_t SetCard(uint32_t id, String path, uint8_t mode);
        String GetPath(uint32_t id);
        uint8_t GetMode(uint32_t id);
        void GetPosition(uint32_t id, uint16_t & file, uint32_t & pos);
        void SetPosition(uint32_t id, uint16_t file, uint32_t pos);
        bool Configured(uint32_t id);
    private:
        static RFIDCardManager* m_Instance;
        RFIDCardManager();
        RFIDCardManager(RFIDCardManager& other) {};
        RFIDCardManager& operator=(RFIDCardManager& other) {return other;};
        void SaveCard(uint32_t id);

        Preferences m_Preferences;
        MultimediaRFIDCollection m_RFIDCollection;
};

class RFIDCardReader
{
    public:
        RFIDCardReader(uint8_t chipSelectPin, uint8_t resetPowerDownPin);
        ~RFIDCardReader();
        Card GetCurrentCard();
        void SetOutdata(const byte* data);
        void DataExchange();
    private:
        MFRC522* m_MFRC522;
        static void ThreadFunction(void* pvParameters);
        void WriteDataToBlock(int blockNum, byte blockData[], MFRC522::MIFARE_Key & key);
        void ReadDataFromBlock(int blockNum, byte readBlockData[], MFRC522::MIFARE_Key & key);
        
        bool PICC_IsCardPresent();
        void* m_ThreadHandle;
        bool m_ThreadRunning;
        Card m_CurrentCard;
        SemaphoreHandle_t m_Mutex;
        uint8_t m_NumTries;
        byte m_BlockData[16];
        bool m_GotOutData;
};

#endif /* RFID_H */
