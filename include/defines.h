
#ifndef DEFINES_H
#define DEFINES_H

#include <stdint.h>
#include <vector>
#include <map>
#include <WString.h>
#include <esp32-hal.h>

typedef uint8_t byte;

//Used pins
#define MFRC_SS_PIN      5
#define MFRC_RST_PIN    14

#define SD_SS_PIN       15

#define I2S_DOUT_PIN    22
#define I2S_BCLK_PIN    26
#define I2S_LRC_PIN     25
#define HEADPHONES_PIN  21

#define CHARGER_PIN     27
#define BAT_STATE_PIN   33
#define BAT_QUERY_PIN   32
#define SHUTDOWN_PIN    12

#define LED1_PIN        17
#define LED2_PIN        16
#define VOLUME_PIN      39
#define TOUCH1_PIN      2
#define TOUCH2_PIN      4

//Used channels of the ESP32 for LEDs
#define LED_CHANNEL1     0
#define LED_CHANNEL2     1

//Defines for RFID
struct Card
{
    uint32_t ID;
    byte BlockData[16];
    bool changed;
};

struct MultimediaRFID
{
    String path;
    uint8_t mode;
    uint16_t c_file;
    uint32_t c_pos;
    uint32_t ID;
    uint32_t num;
    unsigned long lastseen;
};

#define TIME_TO_SAVE    1000
#define SAVE_TIME_OUT   30000
#define THRS_REWIND     10
#define TOUCH_THRS      40
#define HOLD_THRS       1000
#define FASTFOWARD_TIME 2
#define IDLE_TIME_OUT   60000

typedef std::map<uint32_t, MultimediaRFID> MultimediaRFIDCollection;

#define STACK_SIZE      10000
#define RFID_WAIT       200
#define NUM_TRIES       2
#define NO_CARD_ID      (uint32_t)0xFFFFFFFF
#define SIZE_BUFFER     18
#define MAX_SIZE_BLOCK  16

#define CONFIG_BYTE     0
#define MODE_BYTE       5
#define WEBPORTAL_BYTE  6
#define IS_CONFIGURED   0x2A

#define BLOCK_ID        2

extern Card NO_CARD;

struct fileDescriptor
{
  String name;
  int size;
};

typedef std::vector<String> strVec;
typedef std::vector<fileDescriptor> fileVec;

struct fileSorter 
{
    std::map<String, int> fileOrder;
    fileSorter(const std::map<String, int>& order) : fileOrder(order) {}
    bool operator()(fileDescriptor const& order1, fileDescriptor const& order2) const 
    {
      int a1 = fileOrder.at(String(order1.name));
      int b1 = fileOrder.at(String(order2.name)); 
      
      return  a1 < b1;
    }
};

extern SemaphoreHandle_t SPIMutex;

//Defaults

#define DEFAULT_HOSTNAME        "SophiBox"
#define DEFAULT_PASSWORD        ""
#define DEFAULT_POWER_TIMEOUT   300
#define DEFAULT_MAX_VOL         50
#define DEFAULT_MAX_VOL_HP      50

#define VOLTAGE_MAX 4200
#define VOLTAGE_MIN 3300
#define ADC_REFERENCE 3300
#define R2 33
#define R1 10
#define VOLTAGE_OUT(Vin) (((Vin) * R2) / (R1 + R2))
#define VOLTAGE_TO_ADC(in) ((4096 * (in)) / ADC_REFERENCE)
#define BATTERY_MAX_ADC VOLTAGE_TO_ADC(VOLTAGE_OUT(VOLTAGE_MAX))
#define BATTERY_MIN_ADC VOLTAGE_TO_ADC(VOLTAGE_OUT(VOLTAGE_MIN))

#endif /* DEFINES_H */
